﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerPlayer : MonoBehaviour
{
    public float speed;
    public Vector3 dir;
    public Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            rb.velocity = speed * Vector3.forward;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            rb.velocity = speed * -Vector3.forward;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            rb.velocity = speed * -Vector3.right;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            rb.velocity = speed * Vector3.right;
        }
       
    }
    
}
